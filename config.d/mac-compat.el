(defun darwin? ()
  (eq system-type 'darwin))

(defun linux? ()
  (eq system-type 'gnu/linux))

(defun fixed-buffer-width ()
  (cond ((not window-system) 78)
        (t                   100)))

;;; Toolbar setup
(when (not (darwin?))
  (display-time))

(when (darwin?)
  (setenv "PATH"
          (concat (getenv "PATH")
                  ":/usr/local/bin"))
  (setq exec-path (append exec-path '("/usr/local/bin"))))
