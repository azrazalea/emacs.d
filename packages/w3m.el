;;; Web stuff.
(use-package w3m
  :bind (("<f6> w" . 'browse-url-at-point)
         ("<f6> f" . 'browse-url-at-point-firefox)
         ("<f6> d" . 'browse-url-at-point-default)
         ("<f6> k f" . 'browse-url-from-kill-firefox)
         ("<f6> k d" . 'browse-url-from-kill-default))
  :config
  (setq w3m-coding-system 'utf-8
        w3m-file-coding-system 'utf-8
        w3m-file-name-coding-system 'utf-8
        w3m-input-coding-system 'utf-8
        w3m-output-coding-system 'utf-8
        w3m-terminal-coding-system 'utf-8
        w3m-use-cookies t))

(defun browse-url-at-point-firefox ()
  (interactive)
  (let ((browse-url-browser-function 'browse-url-firefox))
    (browse-url-at-point)))

(defun browse-url-at-point-default ()
  (interactive)
  (let ((browse-url-browser-function 'browse-url-default-browser))
    (browse-url-at-point)))

(defun browse-url-from-kill ()
  (interactive)
  (browse-url (current-kill 0 t)))

(defun browse-url-from-kill-default ()
  (interactive)
  (let ((browse-url-browser-function 'browse-url-default-browser))
    (browse-url-from-kill)))

(defun browse-url-from-kill-firefox ()
  (interactive)
  (let ((browse-url-browser-function 'browse-url-firefox))
    (browse-url-from-kill)))
