;; Company mode
(use-package company
  :diminish company-mode)

(use-package company-quickhelp
  :diminish company-quickhelp-mode
  :config
  (company-quickhelp-mode 1))
