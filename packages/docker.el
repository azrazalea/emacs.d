(use-package dockerfile-mode
  :mode "Dockerfile\\'")

(use-package docker
  :bind ("C-c d" . docker))
