;;; Org Mode
(use-package org
  :hook org-indent-mode
  :config (define-key org-mode-map (kbd "C-c C-r") verb-command-map))
