(use-package flycheck
  :diminish t
  :config
  (global-flycheck-mode))

(defun my/use-eslint-from-node-modules ()
  (let ((root (locate-dominating-file
               (or (buffer-file-name) default-directory)
               (lambda (dir)
                 (let ((eslint (expand-file-name "node_modules/eslint/bin/eslint.js" dir)))
                  (and eslint (file-executable-p eslint)))))))
    (when root
      (let ((eslint (expand-file-name "node_modules/eslint/bin/eslint.js" root)))
        (setq-local flycheck-javascript-eslint-executable eslint)))))

(add-hook 'flycheck-mode-hook #'my/use-eslint-from-node-modules)

(require 'subr-x)
(defun my/use-ruby-stuff-from-bundle ()
  (setq-local flycheck-command-wrapper-function
              (lambda (command)
                (append '("bundle" "exec") command)))
  (let ((rubocop (string-trim (shell-command-to-string "bundle exec which rubocop"))))
    (when (file-executable-p rubocop)
      (setq-local flycheck-ruby-rubocop-executable rubocop))))

(add-hook 'ruby-mode-hook #'my/use-ruby-stuff-from-bundle)
