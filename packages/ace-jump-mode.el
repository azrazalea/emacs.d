;;; Ace Jump Mode
(use-package ace-jump-mode
  :bind (("C-'" . ace-jump-mode)
         ("C-M-'" . ace-jump-mode-pop-mark)))
