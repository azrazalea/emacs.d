;;; Projectile
(use-package projectile
  :diminish projectile-mode
  :config
  (projectile-global-mode)
  (helm-projectile-on))
