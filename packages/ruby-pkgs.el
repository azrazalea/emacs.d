;;; Ruby
(use-package inf-ruby
  :ensure t
  :hook (ruby-mode . inf-ruby-minor-mode)
  :config
  (inf-ruby-switch-setup))

(use-package robe
  :diminish robe-mode
  :hook (ruby-mode . robe-mode))

(use-package rspec-mode
  :diminish rspec-mode)

(use-package ruby-electric)

(use-package yari)

(use-package rbenv
  :config
  (global-rbenv-mode))

(use-package haml-mode)

(use-package scss-mode)

(use-package less-css-mode)

(use-package ruby-mode
  :mode ("Gemfile\\'"
         "Gemfile\\.lock\\'"
         "Guardfile\\'"
         "Rakefile\\'"
         "\\.gemspec\\'"
         "\\.rabl\\'"))

(use-package rubocop
  :hook (ruby-mode . rubocop-mode))
