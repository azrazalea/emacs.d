;; Yasnippet
(use-package yasnippet
  :ensure t
  :diminish yas-minor-mode
  :diminish yas-global-mode
  :config
  (add-to-list 'load-path
               "~/.emacs.d/plugins/yasnippet")
  (yas-global-mode 1))
