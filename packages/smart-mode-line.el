;;; Mode-line
(use-package smart-mode-line
  :config
  (sml/setup)
  (add-to-list 'sml/replacer-regexp-list '("^:Git:\\([^/]*\\)/" ":\\1:"))
  (add-to-list 'sml/replacer-regexp-list '("^~/git_repos/" ":Git:")))
