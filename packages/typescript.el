(require 'web-mode)
(defun setup-tide-mode ()
  (interactive)
  (tide-setup)
  (eldoc-mode +1)
  (tide-hl-identifier-mode +1))
(use-package tide
  :demand t
  :hook ((before-save . tide-format-before-save))
  :mode ("\\.tsx\\'" . web-mode)
  :config (flycheck-add-next-checker 'typescript-tide 'javascript-eslint 'append)
   (flycheck-add-next-checker 'tsx-tide 'javascript-eslint 'append)
  (flycheck-add-mode 'typescript-tide 'web-mode)
  (flycheck-add-mode 'javascript-eslint 'web-mode)
  (add-hook 'web-mode-hook
          (lambda ()
            (when (string-equal "tsx" (file-name-extension buffer-file-name))
              (setup-tide-mode))))
  (add-hook 'typescript-mode-hook 'setup-tide-mode))
(setq web-mode-enable-auto-pairing t)
