;;; Clojure
(use-package clojure-mode)

(use-package cider
  :hook cider-turn-on-eldoc-mode)
