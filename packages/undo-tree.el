(use-package undo-tree ; hierarchical undo/redo history
  :config (global-undo-tree-mode)
  :bind (("C-z" . undo-tree-undo)
         ("C-S-z" . undo-tree-redo))
  :diminish undo-tree-mode)
