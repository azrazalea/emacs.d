(use-package rust-mode)

(use-package toml-mode
  :hook (rust-mode . flyspell-prog-mode))

(use-package cargo
  :hook (rust-mode . cargo-minor-mode))
