;;; Rainbow delimiters
(use-package rainbow-delimiters
  :hook ((prog-mode . rainbow-delimiters-mode)
         (cider-repl-mode . rainbow-delimiters-mode)))

(use-package rainbow-identifiers
  :hook (prog-mode . rainbow-identifiers-mode))
