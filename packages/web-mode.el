;;; Web mode
(use-package web-mode
  :mode (("\\.erb\\'" . web-mode)
         ("\\.html\\'" . web-mode)
         ("\\.tmpl\\'" . web-mode)))
